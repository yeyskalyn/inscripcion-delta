console.log("Starting iframe height reports...");

const container = document.querySelector('.main-wrapper')

function sendHeight() {
  if (parent.postMessage) {
    const height = container.scrollHeight;
    // console.log('Height: ', height);    
    // We send { height: number } to the parent
    parent.postMessage({ height }, "*");
  } else {
    console.error("Unable to send messages to parent")
  }
}

// Observe the resize of the window
const resizeObserver = new ResizeObserver(() => {
  sendHeight();
  console.log('Size changed');
});

// Send initial height
sendHeight();

// Start listening resize events
resizeObserver.observe(document.body);