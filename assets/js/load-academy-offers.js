"use strict";
(function () {
    const mainContainer = document.getElementById("academy-offer-container");
    const ofertaId = Number(mainContainer.dataset.ofertaAcademicaId);
    if (ofertaId == null) {
        throw new Error("'data-oferta-academica-id' is not set");
    }
    // Load the json file
    fetch('assets/data/ofertas-academicas.json')
        .then((res) => res.json())
        .then((json) => json.data)
        .then(data => {
        for (const e of data) {
            if (e.id === ofertaId) {
                return render(e);
            }
        }
        throw new Error(`Cannot find oferta academica with id: ${ofertaId}`);
    });
    // Render the page inserting the HTML
    function render(oferta) {
        // Set background image
        const courseSection = mainContainer.querySelector(".courses-section");
        courseSection.style.backgroundImage = `url(${oferta.banner})`;
        // Set banner title
        const courseName = mainContainer.querySelector(".academy-offer-name");
        courseName.innerText = oferta.nombre;
        for (const programa of oferta.programas) {
            const htmlColumns = programa.data.map(e => createColumn(e.tipo, e));
            const container = createContainer(programa.titulo, htmlColumns.join("\n"));
            const htmlContainer = document.createElement("div");
            htmlContainer.innerHTML = container;
            mainContainer.appendChild(htmlContainer);
        }
        if (location.hash.slice(1).length > 0 && location.href !== location.hash) {
            setTimeout(() => {
                location.href = location.hash;
            }, 100);
        }
    }
    function createContainer(name, content) {
        return `
      <!-- ${name.toUpperCase()} -->
      <section class="container">
          <div class="section-heading d-flex align-items-center direction-ltr">
              <div class="heading-content">
                  <h2 class="header-left">
                    <a href="#${name.toLocaleLowerCase()}">
                      <span class="text-weight" id="${name.toLocaleLowerCase()}">${name}</span> 
                    </a>
                  </h2>
              </div> 
          </div>
          <div class="row">
            ${content}
          </div>
      </section>`;
    }
    function createColumn(title, programa) {
        return `
      <div class="col-lg-3 col-md-6 col-sm-12 d-flex flex-wrap">
              <div class="popular-course">
                <div class="courses-head">
                  <div class="courses-img-main">
                    <img src="${getImage(programa)}" class="img-fluid w-100" />
                  </div>
                </div>
                <div class="courses-body">
                  <div class="courses-ratings my-xl-3">
                    <!-- <p class="mb-1">${title}</p> -->
                    <a class="mb-0 stretched-link" href="${programa.path}">${programa.nombre}</a>
                  </div>
                </div>
                <div class="courses-border"></div>
              </div>
            </div>`;
    }
    function getImage(programa) {
        if (programa.foto) {
            return `assets/images/programas/${programa.foto}`;
        }
        switch (programa.tipo.toLocaleLowerCase()) {
            case "curso":
                return "assets/images/programas/curso-800x400.jpg";
            case "diplomado":
                return "assets/images/programas/diplomado-800x400.jpg";
            case "taller":
                return "assets/images/programas/taller-800x400.jpg";
            case "especialidad":
                return "assets/images/programas/especialidad-800x400.jpg";
            case "maestria":
                return "assets/images/programas/maestria-800x400.jpg";
            default:
                return "assets/images/group.jpg";
        }
    }
})();
