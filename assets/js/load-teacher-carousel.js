"use strict";
(function () {
    // Number of docentes to select
    const SAMPLES = 20;
    // Fetch data
    fetch("assets/data/docentes.json")
        .then(res => res.json())
        .then(json => json.data)
        .then(data => render(data));
    const instructorCarousel = document.querySelector("#instructors-carousel");
    const carouselInner = instructorCarousel?.getElementsByClassName("carousel-inner")[0];
    function render(docentes) {
        // Select the teachers to show
        const docentesSeleccionados = [];
        while (docentesSeleccionados.length < SAMPLES) {
            const index = randomRange(0, docentes.length);
            const next = docentes.splice(index, 1)[0];
            docentesSeleccionados.push(next);
        }
        // Insert in the carousel
        insertOnCarousel(docentesSeleccionados);
    }
    function insertOnCarousel(docentes, count = 2) {
        console.assert(count > 0);
        for (let i = 0; i < docentes.length; i += count) {
            const active = i === 0;
            const carouselItem = createCarouselItem(docentes.slice(i, i + count), active);
            const element = createElement(carouselItem);
            carouselInner.insertAdjacentElement("afterbegin", element);
        }
    }
    function createCarouselItem(docentes, active = false) {
        console.assert(docentes.length > 0);
        const items = docentes.map(e => createCard(e)).join("\n");
        return `
        <div class="carousel-item ${active ? "active" : ""}">
            <div class="row">
            ${items}            
            </div>
        </div>
        `;
    }
    function createCard(docente) {
        const fullName = getFullName(docente);
        return `<div class="col-lg-6 col-md-12 col-sm-12">
        <div class="instructor-card">
          <div class="instructor-profile align-items-center">
            <div class="instructor-img">
              <img src="assets/images/docentes/${docente.foto}" />
            </div>
            <div class="instructor-desc">
              <h3 class="mb-0 text-warning">${fullName}</h3>
              <p class="text-primary">${docente.ocupacion}</p>
              <!-- <p>
                I've been involved in teaching and education for more than
                ten years. Always eager to learn, I invested a lot of my
                time in learning…
              </p> -->
            </div>
            <a class="stretched-link" href="docentes/${docente.id}.html"></a>
          </div>
        </div>
      </div>`;
    }
    function createElement(html) {
        const div = document.createElement("div");
        div.innerHTML = html;
        return div.firstElementChild;
    }
    function randomRange(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
    function getFullName(docente) {
        return docente.honorifico ? `${docente.honorifico}. ${docente.nombre}` : docente.nombre;
    }
})();
