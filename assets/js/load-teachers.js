"use strict";
(function () {
    // Container for the cards
    const container = document.getElementById("teacher-container");
    // const scrollBtn = document.querySelector(".scroll-btn") as HTMLButtonElement;
    // let isScrollBtnVisible = false;
    // scrollBtn.addEventListener("click", () => {
    //   scrollTo({
    //     behavior: 'smooth',
    //     top: 0
    //   })
    // })
    // window.onscroll = () => {
    //   if (window.pageYOffset > 20) {
    //     if (!isScrollBtnVisible) {
    //       scrollBtn.style.opacity = "1";
    //       isScrollBtnVisible = true;
    //     }
    //   }
    //   else {
    //     if (isScrollBtnVisible) {
    //       scrollBtn.style.opacity = "0";
    //       isScrollBtnVisible = false;
    //     }
    //   }
    // }
    // Fetch data
    fetch("assets/data/docentes.json")
        .then(res => res.json())
        .then(json => json.data)
        .then(data => render(data));
    // Insert the HTML
    function render(docentes) {
        docentes.sort((a, b) => {
            return b.nombre.localeCompare(a.nombre);
        });
        for (const e of docentes) {
            const card = createCard(e);
            container.insertAdjacentElement('afterbegin', card);
        }
    }
    function createCard(docente) {
        const fullName = getFullName(docente);
        const div = document.createElement("div");
        div.innerHTML = `
        <div class="docente-col direction-rtl">
        <div class="instructor-card" tabindex="0">
          <div class="instructor-profile align-items-center">
            <div class="instructor-img">
              <img src="assets/images/docentes/${docente.foto}" />
            </div>
            <div class="instructor-desc">
              <h3 class="mb-0 text-warning">${fullName}</h3>
              <p class="text-primary">${docente.ocupacion}</p>
            </div>
            <a class="stretched-link" href="docentes/${docente.id}.html"></a>
          </div>
        </div>
      </div>`;
        return div.firstElementChild;
    }
    function getFullName(docente) {
        return docente.honorifico ? `${docente.honorifico}. ${docente.nombre}` : docente.nombre;
    }
})();
